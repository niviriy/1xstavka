public class Constants {

    public static String baseUrl;

    static {
        String serverUrl = System.getenv("baseUrl");
        baseUrl = serverUrl != null ? serverUrl : "https://1xstavka.ru/line/Football/";
    }

}
