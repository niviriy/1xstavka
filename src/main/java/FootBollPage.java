import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import com.codeborne.selenide.WebDriverRunner;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class FootBollPage {
    private static final int TIMEOUT = 10000;

    public static void goToFootBollPage() {
        goToUrl(Constants.baseUrl);
    }

    public static void goToUrl(@Nonnull String url) {
        open(url);
    }

    public static boolean isFootBollPresence() {
        waitAppear(getFootBollElement());
        return getFootBollElement().exists();
    }

    public static boolean waitAppear(@Nonnull SelenideElement selenideElement) {
        return selenideElement.shouldBe(Condition.visible, Duration.ofMillis(TIMEOUT)).isEnabled();
    }

    public static class Championship {


        public static List<String> getMatches() {
            return WebDriverRunner.getWebDriver()
                    .findElements(byXpath("//span[@class='c-events__teams']"))
                    .stream()
                    .map(e -> e.getAttribute("title"))
                    .collect(Collectors.toList());
        }
        public static List <MatchObj>getMatchObjCollection() {
            List <MatchObj>matchObjCollection = new ArrayList();
            List<String> mat = FootBollPage.Championship.getMatches();

            for(String m: mat) {
                MatchObj matchObj = new MatchObj();
                matchObj.setMatch(String.valueOf(m));
                matchObj.setChampionship(getChampionshipElement(String.valueOf(m)).getText());
                matchObj.setTime(getTimeElement(String.valueOf(m)).innerText());
                matchObjCollection.add(matchObj);
            }
            return matchObjCollection.stream().sorted(Comparator.comparing(MatchObj::getTime)).collect(Collectors.toList());
        }
        public Map countByStreamGroupBy(List getMatchObjCollection) {
            return (Map) getMatchObjCollection.stream().collect(Collectors.groupingBy(k -> k, Collectors.counting()));
        }

    }

    private static SelenideElement getFootBollElement() {
        return $(byXpath("//*[@class='b-filters__item switches__item b-filters__item active' and descendant::*[contains(text(), 'Football')]]"));

    }
    private static SelenideElement getChampionshipElement(@Nonnull String match) {
        String xpath = String.format("//*[@class='c-events__liga' and ancestor::*[@data-name='dashboard-champ-content' and descendant::*[@title=\"%s\"]]]", match);
        return $(byXpath(xpath));
    }
    private static SelenideElement getTimeElement(@Nonnull String match) {
        String xpath = String.format("//*[@class='c-events__time min' and ancestor::*[@class='c-events__item c-events__item_col' and descendant::*[@title=\"%s\"]]]", match);
        return $(byXpath(xpath));
    }

}
