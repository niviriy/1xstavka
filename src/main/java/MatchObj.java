import lombok.*;

@Builder(access = AccessLevel.PUBLIC)
@AllArgsConstructor
@NoArgsConstructor
@Data
public class MatchObj {
    private String championship;
    private String match;
    private String time;
}
