import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

class MatchTest {

    @Test
    public void MatchTestFootBoll() {
        FootBollPage.goToFootBollPage();
        Assertions.assertTrue(FootBollPage.isFootBollPresence());
        List<MatchObj> matchObjs = FootBollPage.Championship.getMatchObjCollection();
        Map<String, Long> map = new HashMap<>();
        for (MatchObj champ : matchObjs) {
            map.merge(String.valueOf(champ.getChampionship()), 1L, Long::sum);
        }
        map.forEach((key, value) -> System.out.println(key + " - " + value));
//        List<MatchObj> itemsNamedSam = matchObjs.stream()
//                .filter(item -> item.getChampionship())
//                .collect(Collectors.toList());
        matchObjs.forEach(System.out::println);
        FootBollPage.goToFootBollPage();
    }
}
